package io.hei.eventhei.ui.bde;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.hei.eventhei.Modele.Evenement;
import io.hei.eventhei.R;
import io.hei.eventhei.Utils.SingletonEvenement;

public class BdeFragment extends Fragment {

    View v;
    private  RecyclerView bdeRecyclerView;
    private List<Evenement> listBdeEvenements;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_bde, container, false);
        bdeRecyclerView= v.findViewById(R.id.recyclerview_bde);



        // Adapter
        bdeAdapter adapter = new bdeAdapter(getContext(),listBdeEvenements);

        // Layout Manager
        bdeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        bdeRecyclerView.setAdapter(adapter);

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listBdeEvenements = SingletonEvenement.getInstance().getListBdeEvenement();
    }
}

