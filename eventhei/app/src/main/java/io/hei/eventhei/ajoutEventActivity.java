package io.hei.eventhei;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.hei.eventhei.Utils.DatabaseEvenementManager;
import io.hei.eventhei.Utils.PasserelleEvenement;

public class ajoutEventActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText ajoutEditText,ajoutDescription,ajoutParticipants;
    Spinner ajoutSpinner;
    DatePicker ajoutDate;
    Calendar c;
    Button ajoutEventBoutton,arrowBack;
    DatabaseEvenementManager database;
    TextView testText;
    String currentDate;
    AlertDialog.Builder builder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_event);
        toolbar = findViewById(R.id.toolbar_description);

        ajoutEditText = findViewById(R.id.editText);
        ajoutSpinner = findViewById(R.id.spinner1);
        ajoutDate = findViewById(R.id.Date);
        c = Calendar.getInstance();
        ajoutDescription = findViewById(R.id.TextDescription);
        ajoutParticipants = findViewById(R.id.NbParticipants);
        ajoutEventBoutton = findViewById(R.id.ajoutEventBoutton);
        arrowBack = findViewById(R.id.arrow_back);

        ajoutDate.setMinDate(c.getTimeInMillis());



        database = new DatabaseEvenementManager(this);

        testText = findViewById(R.id.testText);

        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        builder = new AlertDialog.Builder(this);
        builder.setTitle("Partage l'évènement !");
        builder.setMessage("Choisissez votre moyen de partage");
        builder.setIcon(R.drawable.ic_share_black_24dp);
        builder.setNegativeButton("Mail",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto", "abc@hei.yncrea.fr", null));
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, ajoutEditText.getText().toString());
                        emailIntent.putExtra(Intent.EXTRA_TEXT, ajoutDescription.getText().toString());
                        startActivity(Intent.createChooser(emailIntent, "Evenement..."));
                    }
                });

        builder.setNeutralButton("Facebook",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        launchFacebook();
                    }
                });

        builder.setPositiveButton("Quitter",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        overridePendingTransition(R.anim.no_animation, R.anim.slide_down);
                    }
                });


        //builder.setNegativeButtonIcon( getResources().getDrawable(R.drawable.ic_email);
        //builder.setNeutralButtonIcon( getResources().getDrawable(R.drawable.ic_facebook);


        ajoutEventBoutton.setOnClickListener(new View.OnClickListener() {
            Boolean bool = true;

            @Override
            public void onClick(View v) {

                if (validate(ajoutEditText) && validate(ajoutDescription) && validate(ajoutParticipants) && bool) {
                    bool = false;
                    Toast.makeText(getApplicationContext(), "L'évènement à bien été enregistré", Toast.LENGTH_LONG).show();
                    //testtext.setText("resultat \n" + ajoutEditText.getText().toString() + "\n" + CategorieToNumber(ajoutSpinner) + "\n" + DatetoString(ajoutDate) +"\n" + ajoutDescription.getText().toString() + "\n" + ajoutParticipants.getText().toString() + "\n" + currentDate);
                    PasserelleEvenement.ajoutEvenement(ajoutEditText.getText().toString(), DatetoString(ajoutDate), Integer.valueOf(ajoutParticipants.getText().toString()),
                            categorieToUrl(CategorieToNumber(ajoutSpinner)), CategorieToNumber(ajoutSpinner));
                    //http://localhost:8888/ws/wsAddEvent.php?nomEvenement=toto&dateEvenement=2019-11-11&nombreParticipant=10&urlImage=test&numCategorie=1

                    database.insertData(ajoutEditText.getText().toString(), 1);

                    builder.create().show();

                    ajoutEventBoutton.setText(R.string.button_exit);
                } else if (!bool) {
                    finish();
                    overridePendingTransition(R.anim.no_animation, R.anim.slide_down);
                }
            }
        });

        arrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.no_animation, R.anim.slide_down);
            }

        });


    }



    public boolean validate(EditText editText) {
        if (editText.getText().toString().trim().length() < 1) {
            editText.setError("Vérifiez le champ ");
            editText.requestFocus();
            return false;
        }
        return true;
    }

    public String DatetoString(DatePicker datePicker) {
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear() - 1900;

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date d = new Date(year, month, day);
        String strDate = dateFormatter.format(d);

        return strDate;
    }

    public int CategorieToNumber(Spinner spinner) {
        int res;
        if (spinner.getSelectedItem().toString().equals("BDE")) {
            res = 1;
        } else if (spinner.getSelectedItem().toString().equals("BDS")) {
            res = 2;
        } else {
            res = 3;
        }
        return res;
    }

    public String categorieToUrl(int categorie) {
        String res;

        if (categorie == 1) {
            res = "https://cdn.pixabay.com/photo/2017/07/21/23/57/concert-2527495_1280.jpg";
        } else if (categorie == 2) {
            res = "https://cdn.pixabay.com/photo/2017/07/02/19/24/dumbbells-2465478_1280.jpg";
        } else {
            res = "https://cdn.pixabay.com/photo/2015/07/02/10/40/writing-828911_1280.jpg";
        }
        return res;
    }


    public final void launchFacebook() {
        final String urlFb = "fb://page/354672307922615";
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(urlFb));

        // If a Facebook app is installed, use it. Otherwise, launch
        // a browser
        final PackageManager packageManager = getPackageManager();
        List<ResolveInfo> list =
                packageManager.queryIntentActivities(intent,
                        PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() == 0) {
            final String urlBrowser = "https://www.facebook.com/pages/354672307922615";
            intent.setData(Uri.parse(urlBrowser));
        }

        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
