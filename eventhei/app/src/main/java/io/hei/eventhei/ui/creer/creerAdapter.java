package io.hei.eventhei.ui.creer;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.hei.eventhei.Modele.Evenement;
import io.hei.eventhei.R;
import io.hei.eventhei.Utils.SingletonEvenement;
import io.hei.eventhei.descriptionEventActivity;
import io.hei.eventhei.generatorQrcodeActivity;
import io.hei.eventhei.scannerActivity;
import io.hei.eventhei.ui.bds.bdsAdapter;

public class creerAdapter extends RecyclerView.Adapter {
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        return new creerAdapter.ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((creerAdapter.ListViewHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return SingletonEvenement.getInstance().getListCreerEvenement().size();
    }

    private class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView nameText;
        private TextView dateText;

        public ListViewHolder(View itemView){
            super(itemView);
            nameText = (TextView) itemView.findViewById(R.id.name_event);
            dateText = (TextView) itemView.findViewById(R.id.date_event);

            itemView.setOnClickListener(this);


        }

        public void bindView(int position){
            List<Evenement> listEvenement = SingletonEvenement.getInstance().getListCreerEvenement();


            nameText.setText(listEvenement.get(position).getName());
            dateText.setText(listEvenement.get(position).getDate().toString());

        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            Intent intent = new Intent(context, scannerActivity.class);
            context.startActivity(intent);

        }
    }
}
