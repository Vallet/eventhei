package io.hei.eventhei;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import io.hei.eventhei.Utils.DatabaseEvenementManager;

public class descriptionEventActivity extends AppCompatActivity {
    TextView nomDescription,textDescription,dateDescription,categorieDescription;
    Button btnSuivre;
    String txt_nom,txtdate,txt_categorie,txt_participants;
    DatabaseEvenementManager database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_event);

        database = new DatabaseEvenementManager(this);

        Toolbar toolbar = findViewById(R.id.toolbar_description);

        Intent intent =getIntent();

        nomDescription = findViewById(R.id.NomEventList);
        textDescription = findViewById(R.id.descriptionEventList);
        dateDescription = findViewById(R.id.dateEventList);
        categorieDescription = findViewById(R.id.categorieEventList);
        btnSuivre = (Button) findViewById(R.id.DescriptionEventBoutton);

        txt_nom =intent.getStringExtra("Nom");
        txtdate =intent.getStringExtra("Date");
        txt_categorie =intent.getStringExtra("Categorie");
        txt_participants =intent.getStringExtra("NbrParticipants");

        nomDescription.setText(txt_nom);
        textDescription.setText(txt_participants);
        dateDescription.setText(txtdate);

        switch (txt_categorie) {
            case "1":
                categorieDescription.setText("BDE");
                break;
            case "2":
                categorieDescription.setText("BDS");
                break;
            case "3":
                categorieDescription.setText("Administration");
                break;
        }

        btnSuivre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Participation enregistrée",Toast.LENGTH_LONG).show();
                database.insertData(txt_nom,0);

                finish();
            }
        });



        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

}
