package io.hei.eventhei.ui.administration;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.hei.eventhei.Modele.Evenement;
import io.hei.eventhei.R;
import io.hei.eventhei.Utils.SingletonEvenement;

public class adminFragment extends Fragment {

    View v;
    private  RecyclerView adminRecyclerView;
    private List<Evenement> listAdminEvenements;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_admin, container, false);
        adminRecyclerView= v.findViewById(R.id.recyclerview_admin);



        // Adapter
        adminAdapter adapter = new adminAdapter(getContext(),listAdminEvenements);

        // Layout Manager
        adminRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adminRecyclerView.setAdapter(adapter);

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listAdminEvenements = SingletonEvenement.getInstance().getListAdminEvenement();
    }
}

