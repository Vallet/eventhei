package io.hei.eventhei.Modele;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import io.hei.eventhei.Utils.SingletonEvenement;

public class Evenement {
    private int id;
    private String name;
    private Date date;
    private int nbrParticipants;
    private int categorie;

    public Evenement(int id, String name, Date date, int nbrParticipants, int categorie) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.nbrParticipants = nbrParticipants;
        this.categorie = categorie;
    }

    public Evenement(String name, Date date, int nbrParticipants, int categorie) {
        this.name = name;
        this.date = date;
        this.nbrParticipants = nbrParticipants;
        this.categorie = categorie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCategorie() {
        return categorie;
    }

    public void setCategorie(int categorie) {
        this.categorie = categorie;
    }

    public int getNbrParticipants() {
        return nbrParticipants;
    }

    public void setNbrParticipants(int nbrParticipants) {
        this.nbrParticipants = nbrParticipants;
    }


}
