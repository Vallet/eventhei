package io.hei.eventhei;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import io.hei.eventhei.Modele.User;
import io.hei.eventhei.Utils.DatabaseManager;

public class connexionActivity extends AppCompatActivity {
    EditText edtEmail;
    DatabaseManager databaseManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        Button btnConnexion = (Button) findViewById(R.id.btnConnexion);
        edtEmail =  (EditText) findViewById(R.id.edtEmail);
        databaseManager = new DatabaseManager(this);
        final Intent i = new Intent(this,MainActivity.class);
        btnConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkEmail()){
                    Toast.makeText(getApplicationContext(),"gooda",Toast.LENGTH_LONG).show();
                    User user = new User(edtEmail.getText().toString());
                    databaseManager.insertData(edtEmail.getText().toString());
                    startActivity(i);
                }
                else{
                    Toast.makeText(getApplicationContext(),"Verifier votre email",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public boolean checkEmail(){
        return !edtEmail.getText().toString().isEmpty() && edtEmail.getText().toString().contains("@hei.yncrea.fr");
    }
}
