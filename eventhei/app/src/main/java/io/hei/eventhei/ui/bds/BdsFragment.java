

package io.hei.eventhei.ui.bds;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.EventListener;
import java.util.List;

import io.hei.eventhei.Modele.Evenement;
import io.hei.eventhei.R;
import io.hei.eventhei.Utils.PasserelleEvenement;
import io.hei.eventhei.Utils.SingletonEvenement;

public class BdsFragment extends Fragment {
    List<Evenement> listBdsEvenements;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_bds, container, false);
        RecyclerView recyclerView = root.findViewById(R.id.recyclerview_bds);
        recyclerView.setHasFixedSize(true);

        // Layout Manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        // Adapter
        bdsAdapter adapter = new bdsAdapter(getContext(),listBdsEvenements);
        recyclerView.setAdapter(adapter);


        return root;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listBdsEvenements = SingletonEvenement.getInstance().getListBdsEvenement();
    }


}

