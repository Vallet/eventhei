package io.hei.eventhei.ui.suivi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.hei.eventhei.Modele.Evenement;
import io.hei.eventhei.R;
import io.hei.eventhei.Utils.DatabaseEvenementManager;
import io.hei.eventhei.Utils.SingletonEvenement;

public class suiviFragment extends Fragment {
    private List<Evenement> listEvenementsSuivi;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_suivi, container, false);
        RecyclerView recyclerView = root.findViewById(R.id.recyclerview_suivi);
        recyclerView.setHasFixedSize(true);

        // Layout Manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);


        suiviAdapter adapter = new suiviAdapter(getContext(),listEvenementsSuivi);
        recyclerView.setAdapter(adapter);


        return root;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listEvenementsSuivi = SingletonEvenement.getInstance().getListSuiviEvenement();
    }
}
