package io.hei.eventhei.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import io.hei.eventhei.Modele.Evenement;

public class DatabaseEvenementManager extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "heiventEvent.db";
    public static final String TABLE_NAME = "event";
    public static final String COL_1 = "id";
    public static final String COL_2 = "nomEvent";
    public static final String COL_3 = "creer";


    public DatabaseEvenementManager(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME +" (id INTEGER PRIMARY KEY AUTOINCREMENT,nomEvent TEXT,creer INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

    public boolean insertData(String nomEvent,Integer creer) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,nomEvent);
        contentValues.put(COL_3,creer);
        long result = db.insert(TABLE_NAME,null ,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME,null);
        return res;
    }


    public Integer deleteData (String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "id = ?",new String[] {id});
    }
    public List<String> getEventCreer(){
        String selectQuery = "SELECT  * FROM " + TABLE_NAME+" WHERE creer ="+1;
        SQLiteDatabase db  = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<String> lesIdEvent = new ArrayList<>();
        while (cursor.moveToNext()){
            lesIdEvent.add(cursor.getString(1));
        }
        return lesIdEvent;
    }
    public List<String> getEventSuivi(){
        String selectQuery = "SELECT  * FROM " + TABLE_NAME+" WHERE creer ="+0;
        SQLiteDatabase db  = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<String> listEvent = new ArrayList<>();
        while (cursor.moveToNext()){
            listEvent.add(cursor.getString(1));
        }
        return listEvent;
    }
}
