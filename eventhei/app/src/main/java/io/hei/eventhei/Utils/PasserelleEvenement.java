package io.hei.eventhei.Utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.hei.eventhei.Modele.Evenement;

public class PasserelleEvenement {
    public static String urlWS;
    public static String licenceUser;


    public static String getUrlWS() {
        return urlWS;
    }

    public static void setUrlWS(String urlWS) {
        PasserelleEvenement.urlWS = urlWS;
    }

    public static void ajoutEvenement(String nom, String date, int participants, String url, int categorie){
        WebServiceTask monWS = new WebServiceTask();
        monWS.execute("http://10.0.2.2:8888/ws/wsAddEvent.php?nomEvenement=" + nom + "&dateEvenement=" + date +
                "&nombreParticipant=" + participants + "&urlImage=" + url +"&numCategorie=" + categorie);

        //http://localhost:8888/ws/wsAddEvent.php?nomEvenement=toto&dateEvenement=2019-11-11&nombreParticipant=10&urlImage=test&numCategorie=1
    }

    public static void chargerInfoEvennement() {
        List<Evenement> evenements = new ArrayList<>();
        List<Evenement> evenementsBde = new ArrayList<>();
        List<Evenement> evenementsBds = new ArrayList<>();
        List<Evenement> evenementsAdmin = new ArrayList<>();


        WebServiceTask monWS = new WebServiceTask();

        monWS.execute("http://10.0.2.2:8888/ws/wsListEvent.php");

        String resultat = null;
        try {
            resultat = monWS.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        try {
            JSONArray jsonArray = new JSONArray(resultat);
            String nomEvenement;
            int id,nombreParticipant,numCategorie;
            Date dateEvenement;

            for(int i = 0 ; i < jsonArray.length() ; i++) {
                id = Integer.parseInt(jsonArray.getJSONObject(i).getString("idEvenement"));
                nomEvenement = jsonArray.getJSONObject(i).getString("nomEvenement");
                dateEvenement = Date.valueOf(jsonArray.getJSONObject(i).getString("dateEvenement"));
                nombreParticipant = jsonArray.getJSONObject(i).getInt("nombreParticipant");
                numCategorie = jsonArray.getJSONObject(i).getInt("numCategorie");
                evenements.add(new Evenement(id, nomEvenement, dateEvenement, nombreParticipant, numCategorie));
                switch (numCategorie){
                    case 1:
                        evenementsBde.add(new Evenement(id, nomEvenement, dateEvenement, nombreParticipant, numCategorie));
                        break;
                    case 2:
                        evenementsBds.add(new Evenement(id, nomEvenement, dateEvenement, nombreParticipant, numCategorie));
                        break;
                    case 3:
                        evenementsAdmin.add(new Evenement(id, nomEvenement, dateEvenement, nombreParticipant, numCategorie));
                        break;
                }
            }
        } catch (JSONException ignored) {

        }
        SingletonEvenement.getInstance().initListBdeEvenement(evenementsBde);
        SingletonEvenement.getInstance().initListBdsEvenement(evenementsBds);
        SingletonEvenement.getInstance().initListAdminEvenement(evenementsAdmin);
        SingletonEvenement.getInstance().initListEvenement(evenements);
    }


}
