package io.hei.eventhei.ui.bde;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.hei.eventhei.descriptionEventActivity;
import io.hei.eventhei.Modele.Evenement;
import io.hei.eventhei.R;
import io.hei.eventhei.Utils.SingletonEvenement;
import io.hei.eventhei.ui.bds.bdsAdapter;

public class bdeAdapter extends RecyclerView.Adapter<bdeAdapter.MyViewHolder>{

    Context mContext;
    List<Evenement> mData = SingletonEvenement.getInstance().getListBdeEvenement();


    public bdeAdapter(Context mContext, List<Evenement> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }
    @NonNull
    @Override
    public bdeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);
        final bdeAdapter.MyViewHolder viewHolder = new bdeAdapter.MyViewHolder(v);

        //

        viewHolder.item_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent descriptionActivity = new Intent(mContext, descriptionEventActivity.class);

                Evenement event = mData.get(viewHolder.getAdapterPosition());

                descriptionActivity.putExtra("Nom", event.getName());
                descriptionActivity.putExtra("Date", event.getDate().toString());
                descriptionActivity.putExtra("Categorie", String.valueOf(event.getCategorie()));
                descriptionActivity.putExtra("NbrParticipants", String.valueOf(event.getNbrParticipants()));
                mContext.startActivity(descriptionActivity);
            }
        });
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull bdeAdapter.MyViewHolder holder, int position) {

        holder.nameText.setText(mData.get(position).getName());
        holder.dateText.setText(mData.get(position).getDate().toString());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout item_event;
        private TextView nameText;
        private TextView dateText;

        public MyViewHolder(View itemView) {
            super(itemView);

            item_event = (LinearLayout) itemView.findViewById(R.id.item_event);

            nameText = (TextView) itemView.findViewById(R.id.name_event);
            dateText = (TextView) itemView.findViewById(R.id.date_event);
        }
    }
}
