package io.hei.eventhei;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import io.hei.eventhei.Modele.User;
import io.hei.eventhei.Utils.DatabaseManager;

public class MainActivity extends AppCompatActivity {
    DatabaseManager databaseManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        databaseManager = new DatabaseManager(this);

        if(!databaseManager.userExist()){
            Intent i = new Intent(this,connexionActivity.class);
            startActivity(i);
        }
        else{
            Toast.makeText(getApplicationContext(),"Bienvenue",Toast.LENGTH_LONG).show();
            Intent i2 = new Intent(this,ppActivity.class);
            startActivity(i2);
        }
    }
}
