package io.hei.eventhei;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import io.hei.eventhei.Utils.DatabaseEvenementManager;
import io.hei.eventhei.Utils.PasserelleEvenement;
import io.hei.eventhei.Utils.SingletonEvenement;

public class ppActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pp);

        Toolbar toolbar = findViewById(R.id.toolbar);
        MenuItem addEventButton = findViewById(R.id.action_settings);
        setSupportActionBar(toolbar);
        DatabaseEvenementManager databaseEvenementManager = new DatabaseEvenementManager(this);

        PasserelleEvenement.chargerInfoEvennement();
        SingletonEvenement.getInstance().getListEvenementSuiviByName(databaseEvenementManager.getEventSuivi());
        SingletonEvenement.getInstance().getListEvenementCreerByName(databaseEvenementManager.getEventCreer());

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_bde, R.id.nav_bds,R.id.nav_administration,R.id.nav_suivi,R.id.nav_creer)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pp, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.action_settings:
                Intent myIntent = new Intent(ppActivity.this, ajoutEventActivity.class);
                ppActivity.this.startActivity(myIntent);
                overridePendingTransition(R.anim.slide_up,  R.anim.no_animation);
                return true;

            case R.id.action_refresh:
                super.onRestart();
                Intent i = new Intent(ppActivity.this, ppActivity.class);
                startActivity(i);
                finish();
        }
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

}
