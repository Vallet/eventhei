package io.hei.eventhei.ui.creer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import io.hei.eventhei.R;

import io.hei.eventhei.ui.bds.bdsAdapter;
import io.hei.eventhei.ui.suivi.suiviAdapter;

public class creerFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_creer, container, false);
        RecyclerView recyclerView = root.findViewById(R.id.recyclerview_creer);
        recyclerView.setHasFixedSize(true);

        // Layout Manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        // Adapter
        creerAdapter adapter = new creerAdapter();
        recyclerView.setAdapter(adapter);


        return root;
    }
}
