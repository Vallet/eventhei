package io.hei.eventhei.Utils;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import io.hei.eventhei.Modele.Evenement;

public class SingletonEvenement {

    private List<Evenement> listEvenement;
    private Evenement evenement;
    private List<Evenement> listBde;
    private List<Evenement> listBds;
    private List<Evenement> listAdmin;
    private List<Evenement> listEvenementSuivi;
    private List<Evenement> listEvenementCreer;

    private static final SingletonEvenement ourInstance = new SingletonEvenement();

    public static SingletonEvenement getInstance() {
        return ourInstance;
    }

    private SingletonEvenement() {

    }

    public void initListEvenement(List<Evenement> lesEvenenements){
        listEvenement= lesEvenenements;
    }
    public void initListBdeEvenement(List<Evenement> lesEvenenementsBde){
        listBde= lesEvenenementsBde;
    }
    public void initListBdsEvenement(List<Evenement> lesEvenenementsBds){
        listBds= lesEvenenementsBds;
    }
    public void initListAdminEvenement(List<Evenement> lesEvenenementsAdmin){
        listAdmin= lesEvenenementsAdmin;
    }

    public List<Evenement> getListEvenement(){
        return listEvenement;
    }
    public void initEvenement(Evenement new_evenement){
        evenement = new_evenement;
    }

    public List<Evenement> getListBdeEvenement(){
        return listBde;
    }
    public List<Evenement> getListBdsEvenement(){
        return listBds;
    }
    public List<Evenement> getListAdminEvenement(){
        return listAdmin;
    }
    public List<Evenement> getListSuiviEvenement(){return listEvenementSuivi;}
    public List<Evenement> getListCreerEvenement(){return listEvenementCreer;}

    public void getListEvenementSuiviByName(List<String> listOfNameEvent){
            List<Evenement> lesEvenementSuivi = new ArrayList<>();
        for(int i = 0 ; i<listEvenement.size();i++){
            for(int j = 0 ; j < listOfNameEvent.size();j++){
                if(listOfNameEvent.get(j).equals(listEvenement.get(i).getName())){
                    lesEvenementSuivi.add(listEvenement.get(i));
                }
            }
        }
        listEvenementSuivi= lesEvenementSuivi;
    }
    public void getListEvenementCreerByName(List<String> listOfNameEvent){
        List<Evenement> lesEvenementCreer = new ArrayList<>();
        for(int i = 0 ; i<listEvenement.size();i++){
            for(int j = 0 ; j < listOfNameEvent.size();j++){
                if(listOfNameEvent.get(j).equals(listEvenement.get(i).getName())){
                    lesEvenementCreer.add(listEvenement.get(i));
                }
            }
        }
        listEvenementCreer = lesEvenementCreer;
    }


}
