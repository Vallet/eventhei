package io.hei.eventhei.Adapter;

import io.hei.eventhei.Modele.Evenement;

public interface OnRecyclerItemClickListener {
    void onClickView(Evenement evenement);
}
